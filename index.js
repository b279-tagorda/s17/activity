/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/
	let name = "John Doe";
	let age = 25;
	let address = "123 Street, Quezon City";
	let isMarried = false;
	let petName = "Danny";

	function getUserInfo(){
		let userInfo = {name, age, address, isMarried, petName};

		return userInfo;
	}
	let user = getUserInfo();
	console.log("getUserInfo();");
	console.log(user);

/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	

*/
	let Artist1 = "Ben & Ben";
	let Artist2 = "Arthur Nery";
	let Artist3 = "Linkin Park";
	let Artist4 = "Paramore";
	let Artist5 = "Taylor Swift"

	function getArtistArray(){
	let ArtistsArray = [Artist1, Artist2, Artist3, Artist4, Artist5];

	return ArtistsArray;
}
	let Artists = getArtistArray();
	console.log("getArtistsArray();");
	console.log(Artists);

/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/


	let song1 = "Kathang Isip";
	let song2 = "Binhi";
	let song3 = "In the End";
	let song4 = "Brick by Boring Brick";
	let song5 = "Love Story"

	function getSongsArray(){
	let songsArray = [song1, song2, song3, song4, song5];

	return songsArray;
}
	let Songs = getSongsArray();
	console.log("getSongsArray();");
	console.log(Songs);


/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/


	let movie1 = "The Lion King";
	let movie2 = "Meet the Robinsons";
	let movie3 = "Howl's Moving Castle";
	let movie4 = "Tangled";
	let movie5 = "Frozen"

	function getMoviesArray(){
	let moviesArray = [movie1, movie2, movie3, movie4, movie5];

	return moviesArray;
}
	let movies = getMoviesArray();
	console.log("getMoviesArray();");
	console.log(movies);

/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/
	let p1 = "3";
	let p2 = "5";
	let p3 = "7";
	let p4 = "11";
	let p5 = "13"

	function getPrimeNumberArray(){
	let primeArray = [p1, p2, p3, p4, p5];

	return primeArray;
}
	let primeNumbers = getPrimeNumberArray();
	console.log("getPrimeNumberArray();");
	console.log(primeNumbers);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}